/**
* @fileOverview  This file contains the definition of the meta-class mODELcLASS.
* @author Gerd Wagner
* @copyright Copyright 2015 Gerd Wagner, Chair of Internet Technology,
*   Brandenburg University of Technology, Germany.
* @license The MIT License (MIT)
*/
/**
 * Meta-class for creating factory classes with a create method for object creation.
 * Adds the following meta-properties to eNTITYtYPE:
 * + attributesToDisplayInLists: the names of attributes for which a column is
 *       to be created in showAll lists/tables
 * TODO: attributesToDisplayInIdRefColumns: the names of attributes for which a
 *       value is to be included in an IdRef columns. By default [:id[,"name"]].
 *
 * @constructor
 * @this {mODELcLASS}
 * @param {{name: string, supertype: string?, supertypes: string?,
 *          properties: Map,
 *          methods: Map, staticMethods: Map}} slots  The object type definition slots.
 */
var errorTypes = require('error-types');
var eNTITYtYPE = require('eNTITYtYPE');
var ModelClassConstraintViolation = errorTypes.ModelClassConstraintViolation;
var ConstraintViolation = errorTypes.ConstraintViolation;

function mODELcLASS( slots) {
  var properties = {};
  // Any supertype of a mODELcLASS must be a mODELcLASS
  if (slots.supertype && !(slots.supertype instanceof mODELcLASS)) {
    throw new ModelClassConstraintViolation("Supertype "+ slots.supertype +
        " of mODELcLASS "+ this.name +" is not a mODELcLASS!");
  }
  // invoke the eNTITYtYPE constructor since a mODELcLASS is an eNTITYtYPE
  eNTITYtYPE.call( this, slots);
  // Any root mODELcLASS for persistent objects must have a standard ID attribute
  if (!this.supertype && !this.supertypes && !this.isNonPersistent) {
    if (!this.standardId) {
      throw new ModelClassConstraintViolation("Model class "+ this.name +
      " must define a standard identifier attribute!");
    }
  }
  // attributes to display in lists must be properties of the model class
  properties = this.properties;
  if (slots.attributesToDisplayInLists) {
    if (!slots.attributesToDisplayInLists.every( function (attr) {
      return (properties[attr]);
    })) {
      throw new ModelClassConstraintViolation("There is an attribute " +
        "to display in lists, which is not a property of mODELcLASS "+
        this.name +" !");
    } else {
      this.attributesToDisplayInLists = slots.attributesToDisplayInLists;
    }
  }
  // define a property for holding the population in main memory
  this.instances = {};  // a map of all objects of this type
  // add new model class to the mODELcLASS.classes map
  mODELcLASS.classes[this.name] = this;
}
mODELcLASS.prototype = Object.create( eNTITYtYPE.prototype);
mODELcLASS.prototype.constructor = mODELcLASS;
mODELcLASS.classes = {};  // a map of all model classes
/**
 * Factory method for creating new objects as instances of a mODELcLASS.
 * Create new object by assigning the methods defined for the object's type to
 * the object's prototype and using the property descriptors of the property
 * declarations of the object's type for defining its property slots.
 *
 * @method
 * @author Gerd Wagner
 * @param {object} initSlots  The object initialization slots.
 * @return {object}  The new object.
 */
mODELcLASS.prototype.create = function (initSlots) {
  var obj = Object.create( this.methods, this.properties),
      props = this.properties, msg="";
  // add predefined property "type" for direct type
  Object.defineProperty( obj, "type",
      {value: this, writable: false, enumerable: true});
  // initialize object
  Object.keys( initSlots).forEach( function (prop) {
    var val = initSlots[prop];
    // assign property and check its constraints if defined in object type
    if (props[prop]) obj.set( prop, val);
    else if (prop !== 'type') {
      // the pre-defined 'type' property is ignored
      console.log("Undefined object construction property: "+ prop);
    }
  });
  // object-level validation
  if (this.methods.validate) {
    msg = obj.validate();
    if (msg) throw new ConstraintViolation( this.name +
        obj[this.standardId] +": "+ msg);
  }
  // assign initial value to unassigned properties
  Object.keys( props).forEach( function (prop) {
    if (obj[prop] === undefined &&
        props[prop].initialValue !== undefined) {
      obj[prop] = props[prop].initialValue;
    }
  });
  return obj;
};

module.exports = mODELcLASS;