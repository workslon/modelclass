# Overview

NPM module - adapted version of [`mODELcLASS.js` library](https://bitbucket.org/workslon/ontojs/src/8367a54d7dbdc4330a750dfdfbbd9a3e27e17867/src/cOMPLEXtYPE.js) written by Prof. Gerd Wagner, BTU Cottbus

You can find its source code [here](https://bitbucket.org/workslon/modelclass/src/fbd6346e81ab32b42d7e4d4978fbfdf8ed992bf5/index.js)

# Dependencies Graph

![model-class-dependency-graph](assets/model-class-dependency-graph.png)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/modelclass.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/modelclass.git --save
```

# Usage

Please read the [A Quick Tour of mODELcLASSjs](http://web-engineering.info/tech/mODELcLASSjs/validation-tutorial.html#d5e184) on the [http://web-engineering.info/](http://web-engineering.info/).

## CommonJS

```javascript
var mODELcLASS = require('mODELcLASS');
var Book = new mODELcLASS({
  name: "Book",
  properties: {
    isbn: {range:"NonEmptyString", isStandardId: true, label:"ISBN", pattern:/\b\d{9}(\d|X)\b/,
        patternMessage:'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'},
    title: {range:"NonEmptyString", min: 2, max: 50, label:"Title"},
    year: {range:"Integer", min: 1459, max: util.nextYear(), label:"Year"}
  },
  methods: {
    //...
  }
});
var book1 = Book.create({isbn: '123456789X', title: 'Book', year: 2000});
// ...
```

## ES6 Modules

```javascript
import mODELcLASS from 'mODELcLASS';
const Book = new mODELcLASS({
  name: "Book",
  properties: {
    isbn: {range:"NonEmptyString", isStandardId: true, label:"ISBN", pattern:/\b\d{9}(\d|X)\b/,
        patternMessage:'The ISBN must be a 10-digit string or a 9-digit string followed by "X"!'},
    title: {range:"NonEmptyString", min: 2, max: 50, label:"Title"},
    year: {range:"Integer", min: 1459, max: util.nextYear(), label:"Year"}
  },
  methods: {
    //...
  }
});
let book1 = Book.create({isbn: '123456789X', title: 'Book', year: 2000});
// ...
```

